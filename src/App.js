import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom';
import "./App.css";
import {ROUTES} from './routes';
import {connect} from "react-redux";
import {ContactList} from './Components/Contacts/contact-list';
import {ContactAdd} from './Components/Contacts/contact-add';
import {ContactEdit} from './Components/Contacts/contact-edit';
import {ContactDetail} from './Components/Contacts/contact-detail';
import {ContractList} from './Components/Contracts/contract-list';
import {ContractAdd} from './Components/Contracts/contract-add';
import {ContractEdit} from './Components/Contracts/contract-edit';
import {ContractDetail} from './Components/Contracts/contract-detail';
import {Profile} from './Components/profile';
import {Form} from './Components/form';
import {Dashboard} from './Components/dashboard';
import {Header} from './Components/header';
import {SessionModal} from './Components/Modals/sessionModal';
import axios from 'axios';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sessionExpired: false
        };
        let that = this;
        axios.interceptors.response.use(function (response) {
            return response;
        }, function (error) {
            if (401 === error.response.status) {
                that.handleChatche();
            } else {
                return Promise.reject(error);
            }
        });
    }

    handleChatche() {
        this.setState({
            sessionExpired: true
        })
    }

    render() {

        let sessionModal = null;
        let routing = null;

        if (this.state.sessionExpired) {
            sessionModal = <SessionModal loggedIn={this.props.loggedIn} profile={this.props.root.UserLoggedIn}/>
        }

        JSON.parse(this.props.root.UserLoggedIn) ? (
            routing =
                <div className="wrapper">
                    {sessionModal}
                    <Redirect to="/contact"/>
                    <Route path='/' component={(props) => {
                        return <Header direction='/contact' logData='Logout' root={this.props.root}
                                       search={this.props.search}
                                       loggedOut={this.props.loggedIn}
                        />
                    }}/>
                    <Route exact path={ROUTES.CONTACT_LIST} component={(props) => {
                        return <ContactList search={this.props.root.search}/>
                    }}/>
                    <Route exact path={ROUTES.CONTACT_ADD} component={ContactAdd}/>
                    <Route exact path={ROUTES.CONTACT_EDIT} component={ContactEdit}/>
                    <Route exact path={ROUTES.CONTACT_DETAIL} component={ContactDetail}/>

                    <Route exact path={ROUTES.MY_PROFILE} component={(props) => {
                        return <Profile profile={this.props.root.UserLoggedIn}/>
                    }}/>

                    <Route exact path={ROUTES.CONTRACT_LIST} component={(props) => {
                        return <ContractList search={this.props.root.search}/>
                    }}/>
                    <Route exact path={ROUTES.CONTRACT_ADD} component={ContractAdd}/>
                    <Route exact path={ROUTES.CONTRACT_EDIT} component={ContractEdit}/>
                    <Route exact path={ROUTES.CONTRACT_DETAIL} component={ContractDetail}/>
                    <Route  path={ROUTES.DASHBOARD} component={Dashboard}/>
                </div>
        ) : (
            routing =
                <div>
                    <Redirect to="/login"/>
                    <Route exact path="/login" component={(props) => {
                        return <Form loggedIn={this.props.loggedIn}/>
                    }}/>
                </div>
        );

        return (
            <Router>
                    {routing}
            </Router>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        root: state.root
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loggedIn: (log) => {
            dispatch({
                type: "SWITCH_LOGGED_IN",
                payload: log
            });
        },
        search: (terms) => {
            dispatch({
                type: "SEARCH",
                payload: terms
            });
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

