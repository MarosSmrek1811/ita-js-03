import React from "react";
import {Link} from 'react-router-dom';
import {User} from '../Service/user-service';

export class Header extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            initialValue: props.root.search
        }
    }

    handleChange(e) {
        this.props.search(e.target.value);
    }

    handleLogout() {
        this.logout();
    }

    async logout() {
        await User.logout();
        this.props.loggedOut('');
        window.location.href = "http://localhost:3000/login";
    }

    render() {

        return (
            <div>
                <header className="main-header">
                    <Link className="logo" to={this.props.direction}>
                        <span className="logo-mini"><b>A</b>LT</span>
                        <span className="logo-lg"><b>Admin</b>LTE</span>
                    </Link>
                    <nav className="navbar navbar-static-top">
                        <a className="sidebar-toggle" data-toggle="offcanvas" role="button">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </a>
                        <div className="navbar-custom-menu">
                            <ul className="nav navbar-nav">
                                <li><Link to='/contact'>Contact</Link></li>
                                <li><Link to='/contract'>Contract</Link></li>
                                <li className="dropdown user user-menu">
                                    <a className="dropdown-toggle" data-toggle="dropdown">
                                        <img
                                            src="https://www.tm-town.com/assets/default_male300x300-aae6ae0235b6cd78cee8df7ae19f6085.png"
                                            className="user-image" alt=""/>
                                        <span className="hidden-xs">Alexander Pierce</span>
                                    </a>
                                    <ul className="dropdown-menu">
                                        <li className="user-header">
                                            <img
                                                src="https://www.tm-town.com/assets/default_male300x300-aae6ae0235b6cd78cee8df7ae19f6085.png"
                                                className="img-circle"
                                                alt=""/>
                                            <p>
                                                Alexander Pierce
                                                <small>Member since Nov. 2012</small>
                                            </p>
                                        </li>
                                        <li className="user-footer">
                                            <div className="pull-left">
                                                <Link className="btn btn-default btn-flat" to='/profile'>My
                                                    profile</Link>
                                            </div>
                                            <div className="pull-right">
                                                <a onClick={this.handleLogout.bind(this)}
                                                   className="btn btn-default btn-flat">
                                                    {this.props.logData}
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </header>
                <aside className="main-sidebar">
                    <section className="sidebar">
                        <div className="user-panel">
                            <div className="pull-left image">
                                <img
                                    src="https://www.tm-town.com/assets/default_male300x300-aae6ae0235b6cd78cee8df7ae19f6085.png"
                                    className="img-circle" alt=""/>
                            </div>
                            <div className="pull-left info">
                                <p>Alexander Pierce</p>
                                <a><i className="fa fa-circle text-success"></i> Online</a>
                            </div>
                        </div>
                        <form className="sidebar-form" role="search">
                            <div className="input-group">
                                <input autoFocus={true} type="text" className="form-control"
                                       placeholder="Search..."
                                       value={this.state.initialValue}
                                       onChange={this.handleChange.bind(this)}/>
                            </div>
                        </form>
                        <ul className="sidebar-menu">
                            <li className="header">MAIN STUFF</li>
                            <li className="treeview">
                                <Link to='/dashboard'>
                                    <a>
                                        <i className="fa fa-dashboard"></i> <span>Dashboard</span>
                                    </a>
                                </Link>
                            </li>
                        </ul>
                    </section>
                </aside>
            </div>
        )
    }
}




