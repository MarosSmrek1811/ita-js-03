import React from "react";
import {Bar, Line, Pie} from 'react-chartjs-2';
import {ContactsService} from '../Service/contact-service';
import _ from 'lodash';

export class Dashboard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            chartData: {
                labels: ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"]
            }
        }
    }

    componentWillMount() {
        this.getCharData();
    }

    async getCharData() {
        const chardata = await ContactsService.getContacts();
        let newCharMonth = [];
        let numberOfContact = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        chardata.map((m) => {
            newCharMonth.push(this.state.chartData.labels[new Date(m.create.replace(' ', 'T')).getMonth()]);
            ++numberOfContact[new Date(m.create).getMonth()];
        });

        numberOfContact = numberOfContact.filter((n) => {
            return n > 0

        });

        this.setState({
            chartData: {
                labels: _.uniq(newCharMonth),
                datasets: [
                    {
                        label: 'Number of contacts',
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132,1)',
                        borderWidth: 1,
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        hoverBorderColor: 'rgba(255,99,132,1)',
                        data: numberOfContact
                    }
                ]
            }
        });
    }

    render() {
        return (
            <div>
                <div className="content-wrapper" style={{minHeight: "916px"}}>
                    <section className="content-header">
                        <h1>
                            Dashboard
                            <small>charts are awesome</small>
                        </h1>
                    </section>
                    <section className="content">
                        <div className="row">
                            <div className="col-xs-12">

                                <Bar
                                    data={this.state.chartData}
                                    width={100}
                                    height={250}
                                    options={{
                                        maintainAspectRatio: false
                                    }}
                                />

                            </div>
                        </div>
                    </section>
                </div>
            </div>
        );
    }
}
