import React from 'react';
import {User} from '../../Service/user-service';

export class SessionModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            timeToLoggedOut: 60,
            profile: JSON.parse(props.profile)
        };
    }

    componentWillMount() {
        this.timer();
    }

    timer() {
        window.setInterval(() => {
            this.setState({
                timeToLoggedOut: this.state.timeToLoggedOut - 1
            });
            if (this.state.timeToLoggedOut === 0) {
                this.handleLogout();
            }
        }, 1000)
    }

    handleLogout() {
        this.loggedOut()
    }

    async loggedOut() {
        await User.logout();
        this.props.loggedIn('');
        window.location.href = "http://localhost:3000/login";
    }

    handleLoggedIn() {
        this.LoginUser();
    };

    async LoginUser() {
        let values = {username: this.state.profile[1].toString(), password: this.state.profile[3].toString()};
        const user = await User.login(values);
        if (user) {
            this.props.loggedIn(user);
            window.location.href = "http://localhost:3000/contact";
        } else {
            console.log("Something wrong on server");
        }
    }

    render() {
        return (
            <div className="modal modal-warning fade in" id="modal-warning"
                 style={{display: "block", paddingRight: "17px"}}>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">Session Modal</h4>
                        </div>
                        <div className="modal-body">
                            <p>Session expired, please login in or logout</p>
                        </div>
                        <div className="modal-footer">
                            <button data-dismiss="modal" onClick={this.handleLogout.bind(this)} type="button"
                                    className="btn btn-default">
                                Logout
                            </button>
                            <button data-dismiss="modal" onClick={this.handleLoggedIn.bind(this)} type="button"
                                    className="btn btn-danger">Stay logged in {this.state.timeToLoggedOut}s
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
