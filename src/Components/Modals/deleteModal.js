import React from 'react';

export class DeleteModal extends React.Component {

    render() {
        return (
            <div className="modal modal-danger fade in" id="modal-danger">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 className="modal-title">Delete Modal</h4>
                        </div>
                        <div className="modal-body">
                            <p>Sure delete?</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-outline pull-left" data-dismiss="modal">Close
                            </button>
                            <button onClick={this.props.action} type="button"
                                    data-dismiss="modal" className="btn btn-outline">Delete
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
