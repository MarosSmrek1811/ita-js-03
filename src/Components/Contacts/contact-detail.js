import React from 'react';
import {Link} from 'react-router-dom';
import {ContactsService} from '../../Service/contact-service';
import {DeleteModal} from '../Modals/deleteModal';

export class ContactDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    componentWillMount() {
        this.loadContact();
    }

    componentWillReceiveProps(nextProps) {
        this.loadContact();
    }

    async loadContact() {
        const contactDetail = await ContactsService.getContact(this.props.match.params.id);
        this.setState({
            data: contactDetail
        });
    }

    handleDelete() {
        this.deleteContact();
    }

    async deleteContact() {
        await ContactsService.delete(this.state.data.id);
        this.props.history.push('/contact');
    }

    render() {
        return (
            <div>
                <div className="content-wrapper" style={{minHeight: "916px"}}>
                    <section className="content-header">
                        <h1>
                            Contact Detail
                            <small>all your info about {this.state.data.name} </small>
                        </h1>
                    </section>
                    <section className="content">
                        <div className="row">
                            <div className="col-xs-12">
                                <div className="box box-primary">
                                    <div className="box-body box-profile">
                                        <img className="profile-user-img img-responsive img-circle"
                                             src="https://www.tm-town.com/assets/default_male300x300-aae6ae0235b6cd78cee8df7ae19f6085.png"
                                        alt=""/>

                                        <h3 className="profile-username text-center">{this.state.data.name}</h3>

                                        <ul className="list-group list-group-unbordered">
                                            <li className="list-group-item">
                                                <b>Name</b> <a className="pull-right">{this.state.data.name}</a>
                                            </li>
                                            <li className="list-group-item">
                                                <b>Phone</b> <a className="pull-right">{this.state.data.phone}</a>
                                            </li>
                                            <li className="list-group-item">
                                                <b>Adress</b> <a className="pull-right">{this.state.data.address}</a>
                                            </li>
                                            <li className="list-group-item">
                                                <b>Note</b> <a className="pull-right">{this.state.data.note}</a>
                                            </li>
                                        </ul>
                                        <Link className="btn btn-info" to={`/contact/edit/${this.state.data.id}`}>
                                            Edit
                                        </Link>


                                        <button type="button" className="btn btn-danger main-btn" data-toggle="modal"
                                                data-target="#modal-danger">
                                            Delete
                                        </button>
                                        <DeleteModal action={this.handleDelete.bind(this)}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        )
    }
}
;

