import React from "react";
import {Link} from 'react-router-dom';
import {ContactListItem} from "./contact-listItem";
import {ContactsService} from '../../Service/contact-service';
import _ from 'lodash';


export class ContactList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    handleTry() {
        this.try()
    }

    async try() {
        await ContactsService.tryCatchUnauthorized();
    }

    componentWillMount() {
        this.loadContacts();
    }

    async loadContacts() {
        const allData = await ContactsService.getContacts();
        this.setState({
            data: allData
        });
    }

    getSearchContact() {
        return _.filter(this.state.data, d=> JSON.stringify(_.values(d)).toLowerCase().includes(this.props.search.toLowerCase()));
    }

    render() {
        const users = this.getSearchContact().map((user) =>

            <ContactListItem key={user.id} url={`/contact/detail/${user.id}`} name={user.name} phone={user.phone}
                             address={user.address}
                             note={user.note}/>
        );

        return (
            <div>
                <div className="content-wrapper" style={{minHeight: "916px"}}>
                    <section className="content-header">
                        <h1>
                            Contact Tables
                            <small>all your contact</small>
                        </h1>
                    </section>
                    <section className="content">
                        <div className="row">
                            <div className="col-xs-12">
                                <div className="box">
                                    <div className="box-header">
                                        <h3 className="box-title">Contact Table With Some Features</h3>
                                    </div>

                                    <div className="box-body">
                                        <div id="example1_wrapper"
                                             className="dataTables_wrapper form-inline dt-bootstrap">
                                            <div className="row">
                                                <div className="col-sm-6 btn-line">
                                                    <Link to="/contact/add">
                                                        <button className="btn btn-success"> New Contract
                                                        </button>
                                                    </Link>
                                                    <button className="btn btn-warning main-btn" onClick={this.handleTry.bind(this)}>Return 401</button>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <table id="example1"
                                                           className="table table-bordered table-striped dataTable"
                                                           role="grid" aria-describedby="example1_info">
                                                        <thead>
                                                        <tr role="row">
                                                            <th className="sorting_asc" tabIndex="0">Name
                                                            </th>
                                                            <th className="sorting" tabIndex="0">Phone
                                                            </th>
                                                            <th className="sorting" tabIndex="0">Addres
                                                            </th>
                                                            <th className="sorting" tabIndex="0">Note
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        {users}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        );
    }
}

