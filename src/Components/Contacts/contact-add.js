import React from "react";
import {ContactsService} from '../../Service/contact-service';
import {ContactForm} from './contact-form';

export class ContactAdd extends React.Component {

    handleCreate(newData) {
        this.create(newData);
    }

    async create(newData) {
        await ContactsService.create(newData);
        this.props.history.push('/contact');
    }

    render() {
        return (
            <div className="content-wrapper">
                <section className="content-header">
                    <h1>
                        Add New Contact
                        <small>more contacts is better</small>
                    </h1>
                </section>
                <ContactForm handleFormSubmit={this.handleCreate.bind(this)}/>
            </div>
        );
    }
}

