import React from 'react';

export class ContactForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: {}
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            data: nextProps.data
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.handleFormSubmit(this.state.data);
    }

    handleChange(event) {
        this.state.data[event.target.name] = event.target.value;
        this.forceUpdate();
    }

    render() {
        return (
            <div>
                <section className="content">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="box box-primary">
                                <div className="box-header with-border">
                                    <h3 className="box-title">Add new</h3>
                                </div>
                                <form onSubmit={this.handleSubmit.bind(this)} role="form">
                                    <div className="box-body">
                                        <div className="form-group">
                                            <label htmlFor="exampleInputEmail1">Name</label>
                                            <input name="name" autoFocus={true} placeholder="Name"
                                                   value={this.state.data.name || ''}
                                                   onChange={this.handleChange.bind(this)} className="form-control"/>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="exampleInputPassword1">Phone</label>
                                            <input name="phone" placeholder="Phone" value={this.state.data.phone || ''}
                                                   onChange={this.handleChange.bind(this)} className="form-control"/>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="exampleInputPassword1">Addres</label>
                                            <input name="address" placeholder="Address"
                                                   value={this.state.data.address || ''}
                                                   onChange={this.handleChange.bind(this)} className="form-control"/>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="exampleInputPassword1">Note</label>
                                            <textarea name="note" placeholder="Note" value={this.state.data.note || ''}
                                                      onChange={this.handleChange.bind(this)} className="form-control"/>
                                        </div>
                                    </div>
                                    <div className="box-footer">
                                        <button type="submit" className="btn btn-primary">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
};