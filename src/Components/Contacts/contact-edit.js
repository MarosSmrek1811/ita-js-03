import React from 'react';
import {ContactsService} from '../../Service/contact-service';
import {ContactForm} from './contact-form';

export class ContactEdit extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    componentWillMount() {
        this.loadContact();
    }

    async loadContact() {
        const editContact = await ContactsService.getContact(this.props.match.params.id);
        this.setState({
            data: editContact
        })
    }

    handleEdit() {
        this.updateContact();
    }

    async updateContact() {
        await ContactsService.update(this.state.data);
        this.props.history.push(`/contact/detail/${this.state.data.id}`);
    }

    render() {
        return (
            <div className="content-wrapper">
                <section className="content-header">
                    <h1>
                        Update contact
                        <small>actually info is important</small>
                    </h1>
                </section>
                <ContactForm handleFormSubmit={this.handleEdit.bind(this)} data={this.state.data}/>
            </div>
        );
    }
};