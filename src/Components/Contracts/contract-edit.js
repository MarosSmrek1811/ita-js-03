import React from 'react';
import {ContractsService} from '../../Service/contract-service';
import {ContactsService} from '../../Service/contact-service';
import {ContractForm} from './contract-form';

export class ContractEdit extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            contactData: []
        }
    }

    componentWillMount() {
        this.loadContract();
    }

    async loadContract() {
        const editContract = await ContractsService.getContract(this.props.match.params.id);
        const contact = await ContactsService.getContacts();
        this.setState({
            data: editContract,
            contactData: contact
        })
    }

    handleEdit() {
        this.updateContract();
    }

    async updateContract() {
        await ContractsService.update(this.state.data);
        this.props.history.push(`/contract/detail/${this.state.data.id}`);
    }

    render() {
        return (
            <div className="content-wrapper">
                <section className="content-header">
                    <h1>
                        Update contact
                        <small>actually info is important</small>
                    </h1>
                </section>
                <ContractForm contact={this.state.contactData} handleFormSubmit={this.handleEdit.bind(this)}
                              data={this.state.data}/>
            </div>
        );
    }
}
