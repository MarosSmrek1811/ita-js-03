import React from "react";
import _ from 'lodash';
import {Link} from 'react-router-dom';
import {ContractListItem} from "./contract-listItem";
import {ContractsService} from '../../Service/contract-service';

export class ContractList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    componentWillMount() {
        this.loadContracts();
    }

    async loadContracts() {
        const allData = await ContractsService.getContracts();
        this.setState({
            data: allData
        });
    }

    getSearchContract() {
        return _.filter(this.state.data, d => JSON.stringify(_.values(d)).toLowerCase().includes(this.props.search.toLowerCase()));
    }

    render() {
        const users = this.getSearchContract().map((user) =>

            <ContractListItem key={user.id} url={`/contract/detail/${user.id}`} name={user.name} price={user.price}
                              note={user.note}/>
        );

        return (
            <div>
                <div className="content-wrapper" style={{minHeight: "916px"}}>
                    <section className="content-header">
                        <h1>
                            Contract Tables
                            <small>all your contract</small>
                        </h1>
                    </section>
                    <section className="content">
                        <div className="row">
                            <div className="col-xs-12">
                                <div className="box">
                                    <div className="box-header">
                                        <h3 className="box-title">Contract Table With Some Features</h3>
                                    </div>

                                    <div className="box-body">
                                        <div id="example1_wrapper"
                                             className="dataTables_wrapper form-inline dt-bootstrap">
                                            <div className="row">
                                                <div className="col-sm-6 btn-line">
                                                    <Link to="/contract/add">
                                                        <button className="btn btn-success"> New Contract
                                                        </button>
                                                    </Link>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <table id="example1"
                                                           className="table table-bordered table-striped dataTable"
                                                           role="grid" aria-describedby="example1_info">
                                                        <thead>
                                                        <tr role="row">
                                                            <th className="sorting_asc" tabIndex="0">Name
                                                            </th>
                                                            <th className="sorting" tabIndex="0">Price
                                                            </th>
                                                            <th className="sorting" tabIndex="0">Note
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        {users}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        );
    }
}
