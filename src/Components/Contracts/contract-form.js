import React from 'react';

export class ContractForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: {},
            contactData: [],
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            data: nextProps.data,
            contactData: nextProps.contact
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.handleFormSubmit(this.state.data);
    }

    handleSelectChange(event) {
        this.state.contactData[event.target.name] = event.target.value;
        this.forceUpdate();
    }

    handleChange(event) {
        this.state.data[event.target.name] = event.target.value;
        this.forceUpdate();
    }

    render() {

        return (
            <div>
                <section className="content">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="box box-primary">
                                <div className="box-header with-border">
                                    <h3 className="box-title">Add new</h3>
                                </div>
                                <form onSubmit={this.handleSubmit.bind(this)} role="form">
                                    <div className="box-body">
                                        <div className="form-group">
                                            <label htmlFor="exampleInputEmail1">Name</label>
                                            <input name="name" autoFocus={true} placeholder="Name"
                                                   value={this.state.data.name || ''}
                                                   onChange={this.handleChange.bind(this)} className="form-control"/>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="exampleInputEmail1">Contact</label>
                                            <select className="form-control" name="contact_name"
                                                    value={this.state.contactData.contact_name}
                                                    onChange={this.handleSelectChange.bind(this)}>
                                                {this.state.contactData.map(c => <option key={c.id}
                                                                                         value={c.id}>{c.name}</option>)}
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="exampleInputPassword1">Price</label>
                                            <input name="price" placeholder="Price" value={this.state.data.price || ''}
                                                   onChange={this.handleChange.bind(this)} className="form-control"/>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="exampleInputPassword1">Note</label>
                                            <textarea name="note" placeholder="Note" value={this.state.data.note || ''}
                                                      onChange={this.handleChange.bind(this)} className="form-control"/>
                                        </div>
                                    </div>
                                    <div className="box-footer">
                                        <button type="submit" className="btn btn-primary">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
};