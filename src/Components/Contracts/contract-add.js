import React from "react";
import {ContractsService} from '../../Service/contract-service';
import {ContractForm} from './contract-form';

export class ContractAdd extends React.Component {

    handleCreate(newData) {
        this.create(newData);
    }

    async create(newData) {
        await ContractsService.create(newData);
        this.props.history.push('/contract');
    }

    render() {
        return (
            <div className="content-wrapper">
                <section className="content-header">
                    <h1>
                        Add New Contact
                        <small>more contacts is better</small>
                    </h1>
                </section>
                <ContractForm handleFormSubmit={this.handleCreate.bind(this)}/>
            </div>
        );
    }
}

