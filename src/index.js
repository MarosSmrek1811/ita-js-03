import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import 'admin-lte/dist/css/AdminLTE.css';
import 'admin-lte/dist/css/skins/_all-skins.min.css';
import 'admin-lte/dist/js/app.min';
import './index.css';
import {Provider} from "react-redux";
import store from './store';
import axios from 'axios';
axios.defaults.withCredentials = true;

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
