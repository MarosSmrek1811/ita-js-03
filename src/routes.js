export const ROUTES = {
    CONTACT_LIST : '/contact',
    CONTACT_ADD : '/contact/add',
    CONTACT_EDIT : '/contact/edit/:id',
    CONTACT_DETAIL : '/contact/detail/:id',

    CONTRACT_LIST : '/contract',
    CONTRACT_ADD : '/contract/add',
    CONTRACT_EDIT : '/contract/edit/:id',
    CONTRACT_DETAIL : '/contract/detail/:id',

    MY_PROFILE : '/profile',
    DASHBOARD : '/dashboard',
};